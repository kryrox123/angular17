import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css'],
})
export class HijoComponent implements OnInit {
  mensaje = "Mensaje de hijo a padre";
  mensaje2 = "No dejes que nadie te diga que puedes y que no puedes hacer";
  mensaje3 = "Si la vida te la limones, haces limonada";
  numero = 68465465;
  verdad = false;

  @Output() mensajeHijo = new EventEmitter<string>();
  @Output() mensajeHijo2 = new EventEmitter<string>();
  @Output() fraseHijo3 = new EventEmitter<string>();
  @Output() numeroHijo = new EventEmitter<number>();
  @Output() booleanHijo = new EventEmitter<boolean>();
  constructor() {}

  ngOnInit(): void {
    this.mensajeHijo.emit(this.mensaje);
    this.mensajeHijo2.emit(this.mensaje2);
    this.fraseHijo3.emit(this.mensaje3);
    this.numeroHijo.emit(this.numero);
    this.booleanHijo.emit(this.verdad);
  }
}
